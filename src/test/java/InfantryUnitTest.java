import ntnu.idatt2001.wargames.units.InfantryUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InfantryUnitTest {

    InfantryUnit infantryUnit = new InfantryUnit("Infantry", 100, "ha");

    @Test
    void getAttackBonus() {
        assertEquals(2,  infantryUnit.getAttackBonus());
    }

    @Test
    void getResistBonus() {
        assertEquals(1, infantryUnit.getResistBonus());
    }
}
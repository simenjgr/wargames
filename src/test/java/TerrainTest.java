import ntnu.idatt2001.wargames.Terrain;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TerrainTest {

    Terrain terrain = new Terrain();

    @Test
    void getTerrain() {
        terrain.setTerrain("Forest");
        assertEquals("Forest", terrain.getTerrain());    }

    @Test
    void setTerrain() {
        terrain.setTerrain("Plains");
        assertEquals("Plains", terrain.getTerrain());
    }
}
import ntnu.idatt2001.wargames.units.RangedUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangedUnitTest {

    RangedUnit rangedUnit = new RangedUnit("Ranged", 100, "Ranged Unit");

    @Test
    void getAttackBonus() {
        assertEquals(3, rangedUnit.getAttackBonus());
    }

    @Test
    void getResistBonus() {
        assertEquals(6, rangedUnit.getResistBonus());
    }
}
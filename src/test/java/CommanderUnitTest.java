import ntnu.idatt2001.wargames.units.CommanderUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommanderUnitTest {

    CommanderUnit commanderUnit = new CommanderUnit("Commander", 100, "Commander Unit");

    @Test
    void getAttackBonus() {
        assertEquals(6, commanderUnit.getAttackBonus());
    }

    @Test
    void getResistBonus() {
        assertEquals(1, commanderUnit.getResistBonus());
    }
}
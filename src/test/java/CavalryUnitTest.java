import ntnu.idatt2001.wargames.units.CavalryUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CavalryUnitTest {

    CavalryUnit cavalryUnit = new CavalryUnit("Cavalry", 100, "Cavalry Unit");

    @Test
    void getAttackBonus() {
        assertEquals(6, cavalryUnit.getAttackBonus());
    }

    @Test
    void getResistBonus() {
        assertEquals(1, cavalryUnit.getResistBonus());
    }
}
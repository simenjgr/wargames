module Wargame {
    requires javafx.controls;
    requires javafx.fxml;
    exports ntnu.idatt2001.wargames;
    opens ntnu.idatt2001.wargames to javafx.fxml;
    exports ntnu.idatt2001.wargames.controllers;
    opens ntnu.idatt2001.wargames.controllers to javafx.fxml;
    exports ntnu.idatt2001.wargames.units;
    opens ntnu.idatt2001.wargames.units to javafx.fxml;
}
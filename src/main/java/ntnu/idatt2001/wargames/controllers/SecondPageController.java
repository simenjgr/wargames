package ntnu.idatt2001.wargames.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ntnu.idatt2001.wargames.Army;
import ntnu.idatt2001.wargames.Battle;
import ntnu.idatt2001.wargames.FileHandler;
import ntnu.idatt2001.wargames.UnitFactory;
import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * second page controller
 * includes all methods used in the second page of the application
 */
    public class SecondPageController implements Initializable {

        @FXML Button knapp;
        @FXML TextArea textAreaHuman,textAreaOrc;
        @FXML TextField troopAmountHuman,troopAmountOrc;
        @FXML ChoiceBox<String> myChoiceBoxOrc, myChoiceBoxHuman, terrainChoice;
        @FXML Label troopCountHuman,troopCountOrc;
        @FXML Label fileLabelHuman,fileLabelOrc;
        @FXML Label infantryUnitCountOrc,cavalryUnitCountOrc,commanderUnitCountOrc,rangedUnitCountOrc;
        @FXML Label infantryUnitCountHuman,rangedUnitCountHuman,commanderUnitCountHuman, cavalryUnitCountHuman;
        @FXML Label confirmationDialogHuman,confirmationDialogOrc;
        @FXML Label winnerHumans,winnerOrcs,drawHuman,drawOrc;

        private final String [] terrainType =  {"Hill", "Forest", "Plains"};
        private final String [] troopTypeHuman = {"CommanderUnit", "InfantryUnit", "CavalryUnit", "RangedUnit"};
        private final String [] troopTypeOrc = {"CommanderUnit", "InfantryUnit", "CavalryUnit", "RangedUnit"};
        private static final String humanArmyListFilePath = "src/main/resources/armyList/humanArmyList.csv";
        private static final String orcArmyListFilePath = "src/main/resources/armyList/orcArmyList.csv";
        private final Army orcArmy = new Army("Orc army");
        private final Army humanArmy = new Army("Human army");
        private final File fileHuman = new File(humanArmyListFilePath);
        private final File fileOrc = new File(orcArmyListFilePath);
        private final FileChooser fileChooser = new FileChooser();
        private final UnitFactory unitFactory = new UnitFactory();
        private int numberOfTroopsOrc;
        private int numberOfTroopsHuman;

    /**
     * method that allows you to read a .csv file from chosen location and prints in application for humans
     * @param event event
     * @throws FileNotFoundException file not found exception
     */
        @FXML void getTextHuman(MouseEvent event) throws FileNotFoundException {
            try {
                File f = fileChooser.showOpenDialog(new Stage());
                Scanner scanner = new Scanner(f);
                while (scanner.hasNextLine()) {
                    textAreaHuman.appendText(scanner.nextLine() + "\n");
                }
                fileLabelHuman.setTextFill(Paint.valueOf("#00b539"));
                fileLabelHuman.setText("File added!");
            } catch (NullPointerException e) {
                fileLabelHuman.setTextFill(Paint.valueOf("#ab0606"));
                fileLabelHuman.setText("No file added!");
            }
        }

    /**
     * method that allows you to read a .csv file from chosen location and prints in application for orcs
     * @param event event
     * @throws FileNotFoundException file not found exception
     */
        @FXML void getTextOrc(MouseEvent event) throws FileNotFoundException {
            try { File file = fileChooser.showOpenDialog(new Stage());
                Scanner scanner = new Scanner (file);
                while (scanner.hasNextLine()) {
                textAreaOrc.appendText(scanner.nextLine()+ "\n");
            }
                fileLabelOrc.setTextFill(Paint.valueOf("#00b539"));
                fileLabelOrc.setText("File added!");
        }   catch (NullPointerException e) {
                fileLabelOrc.setTextFill(Paint.valueOf("#ab0606"));
                fileLabelOrc.setText("No file added!");
        }
    }

    /**
     * allows you to save your text file to any chosen location on your computer for humans
     * @param event event
     */
        @FXML void saveHuman (MouseEvent event){
           File file = fileChooser.showSaveDialog(new Stage());
           if (file != null) {
            saveSystem(file, textAreaHuman.getText());
           }
        }
    /**
     * allows you to save your text file to any chosen location on your computer for orcs
     * @param event event
     */
         @FXML void saveOrc (MouseEvent event){
        File file = fileChooser.showSaveDialog(new Stage());
        if (file != null) {
            saveSystem(file, textAreaOrc.getText());
        }
    }

    /**
     * logic for saving on your system
     * @param file file
     * @param content string with content
     */
        public void saveSystem (File file, String content) {
            try {
                PrintWriter printWriter = new PrintWriter(file);
                printWriter.write(content);
                printWriter.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    /**
     * method for setting all the label text counters for the orc army
     */
        public void setUnitCountOrc () {
            troopCountOrc.setText(String.valueOf(orcArmy.getAllUnits().size()));
            infantryUnitCountOrc.setText(String.valueOf(orcArmy.getInfantryUnits().size()));
            cavalryUnitCountOrc.setText(String.valueOf(orcArmy.getCavalryUnits().size()));
            commanderUnitCountOrc.setText(String.valueOf(orcArmy.getCommanderUnits().size()));
            rangedUnitCountOrc.setText(String.valueOf(orcArmy.getRangedUnits().size()));
        }

    /**
     * method for setting all the label text counters for the human army
     */
        public void setUnitCountHuman (){
            troopCountHuman.setText(String.valueOf(humanArmy.getAllUnits().size()));
            infantryUnitCountHuman.setText(String.valueOf(humanArmy.getInfantryUnits().size()));
            cavalryUnitCountHuman.setText(String.valueOf(humanArmy.getCavalryUnits().size()));
            commanderUnitCountHuman.setText(String.valueOf(humanArmy.getCommanderUnits().size()));
            rangedUnitCountHuman.setText(String.valueOf(humanArmy.getRangedUnits().size()));
        }

    /**
     * removes and clears diffrent values connected to the orc army
     */
        public void removeTextOrcArmy() {
            textAreaOrc.setText("");
            orcArmy.removeAllUnits();
            numberOfTroopsOrc = 0;
            fileLabelOrc.setText("");
            winnerOrcs.setText("");
            drawOrc.setText("");
            setUnitCountOrc();
         }

    /**
     * removes and clears diffrent values connected to the orc army
     */
        public void removeTextHumanArmy() {
            textAreaHuman.setText("");
            humanArmy.removeAllUnits();
            numberOfTroopsHuman = 0;
            fileLabelHuman.setText("");
            winnerHumans.setText("");
            drawHuman.setText("");
            setUnitCountHuman();
        }

    /**
     * creates an army of orc units and both write and reads this to/from file
     * @throws IOException
     */
        public void createOrcArmy () throws IOException {
            String troopType = myChoiceBoxOrc.getValue();
            String amount = troopAmountOrc.getText();
            Pattern specialCharacters = Pattern.compile("[,.!@#$%&*()_+=|<>?{}\\[\\]~-]");
            Matcher containsSpecialCharacter = specialCharacters.matcher(amount);
            if (containsSpecialCharacter.find()) {
                confirmationDialogOrc.setText("Can't contain special characters!");
            } else if (amount.isEmpty()) {
                confirmationDialogOrc.setText("Please enter a number!");
            } else if (troopType == null) {
                confirmationDialogOrc.setText("You have to choose a troop type first!");
            }
            else {
                try {
            int number = Integer.parseInt(amount);
            unitFactory.createTroops(number, troopType, orcArmy);
            numberOfTroopsOrc = numberOfTroopsOrc+number;
            setUnitCountOrc();
            FileHandler.writeArmytoFile(fileOrc, orcArmy);
            String orcArmy1 = FileHandler.readFromList(orcArmyListFilePath);
            textAreaOrc.setText(String.valueOf(orcArmy1));
            confirmationDialogOrc.setText("");
                } catch (NumberFormatException e) {
                    confirmationDialogHuman.setText("You can only write numbers!");
                }
            }
        }

    /**
     * creates an army of human units and both write and reads this to/from file
     * @throws IOException
     */
        public void createHumanArmy () throws IOException {
            String troopType = myChoiceBoxHuman.getValue();
            String amount = troopAmountHuman.getText();
            Pattern specialCharacters = Pattern.compile("[,.!@#$%&*()_+=|<>?{}\\[\\]~-]");
            Matcher containsSpecialCharacter = specialCharacters.matcher(amount);
            if (containsSpecialCharacter.find()) {
                confirmationDialogHuman.setText("Can't contain special characters!");
            } else if (amount.isEmpty()) {
                confirmationDialogHuman.setText("Please enter a number!");
            } else if (troopType == null) {
                confirmationDialogHuman.setText("You have to choose a troop type first!");
            }
            else {
                try {
                    int number = Integer.parseInt(amount);
                    unitFactory.createTroops(number, troopType, humanArmy);
                    numberOfTroopsHuman = numberOfTroopsHuman + number;
                    setUnitCountHuman();
                    FileHandler.writeArmytoFile(fileHuman, humanArmy);
                    String humanArmy1 = FileHandler.readFromList(humanArmyListFilePath);
                    textAreaHuman.setText(String.valueOf(humanArmy1));
                    confirmationDialogHuman.setText("");
                } catch (NumberFormatException e) {
                    confirmationDialogHuman.setText("You can only write numbers!");
                }
            }
        }

    /**
     * method that simulate a battle between two armies and writes armies to file
     * @throws FileNotFoundException
     */
        public void simulate () throws FileNotFoundException {
            Battle battle = new Battle(humanArmy, orcArmy);
            String troopType = terrainChoice.getValue();
            battle.simulate(troopType);

            FileHandler.writeArmytoFile(fileHuman, humanArmy);
            FileHandler.writeArmytoFile(fileOrc, orcArmy);
            String humanArmy1 = FileHandler.readFromList(humanArmyListFilePath);
            String orcArmy1 = FileHandler.readFromList(orcArmyListFilePath);

            textAreaHuman.setText(String.valueOf(humanArmy1));
            textAreaOrc.setText(String.valueOf(orcArmy1));
            setUnitCountHuman();
            setUnitCountOrc();
            if (humanArmy.hasUnits()) {
            winnerHumans.setText("Winner");
            }
            else if (orcArmy.hasUnits()) {
            winnerOrcs.setText("Winner");
            }
            else if (!orcArmy.hasUnits() && !humanArmy.hasUnits()) {
            drawHuman.setText("Match ended in a draw!");
            drawOrc.setText("Match ended in a draw!");
            }
        }

    /**
     * iniztialize method
     * @param url url
     * @param resourceBundle resource bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        fileChooser.setInitialDirectory(new File("C:"));
        myChoiceBoxHuman.getItems().addAll(troopTypeHuman);
        myChoiceBoxOrc.getItems().addAll(troopTypeOrc);
        terrainChoice.getItems().addAll(terrainType);
        }
}

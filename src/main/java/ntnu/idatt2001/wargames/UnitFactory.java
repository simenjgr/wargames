package ntnu.idatt2001.wargames;

import ntnu.idatt2001.wargames.units.*;

/**
 * Unit factory for creating diffrent types of units upon request
 */

public class UnitFactory {

    /**
     * method that can create more than one troop
     * @param numberOfUnits number of units
     * @param troopType type of troop
     * @param army army to add units to
     */
    public void createTroops (int numberOfUnits, String troopType, Army army) {
        switch (troopType) {
            case "CommanderUnit":
                for (int i = 0; i < numberOfUnits; i++) {
                    army.addUnit(new CommanderUnit("Champion", 100, "Commander unit"));
                }
                break;
            case "InfantryUnit":
                for (int i = 0; i < numberOfUnits; i++) {
                    army.addUnit(new InfantryUnit("Legionnaire", 50, "Infantry unit"));
                }
                break;
            case "CavalryUnit":
                for (int i = 0; i < numberOfUnits; i++) {
                    army.addUnit(new CavalryUnit("Black rider", 100, "Cavalry unit"));
                }
                break;
            case "RangedUnit":
                for (int i = 0; i < numberOfUnits; i++) {
                    army.addUnit(new RangedUnit("Legolas", 100, "Ranged unit"));
                }
                break;
        }
    }

    /**
     * creates one troop based on type health and army
     * @param troopType type of troop
     * @param health health unit
     * @param army army
     */
    public void createOneTroop (String troopType, int health, Army army) {
        switch (troopType) {
            case "CommanderUnit":
                {
                    army.addUnit(new CommanderUnit("Champion", health, "Commander unit"));
                }
                break;
            case "InfantryUnit":
                {
                    army.addUnit(new InfantryUnit("Legionnaire", health, "Infantry unit"));
                }
                break;
            case "CavalryUnit":
                {
                    army.addUnit(new CavalryUnit("Black rider", health, "Cavalry unit"));
                }
                break;
            case "RangedUnit":
                {
                    army.addUnit(new RangedUnit("Legolas", health, "Ranged unit"));
                }
                break;
        }
    }
}

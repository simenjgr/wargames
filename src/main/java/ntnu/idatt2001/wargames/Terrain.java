package ntnu.idatt2001.wargames;

/**
 * class for creating a terrain in battles between two armies and gaining diffrent bonuses to units
 */
public class Terrain {

    private static String terrain;

    /**
     * get method for terrain
     * @return terrain
     */
    public String getTerrain () {
        return terrain;
    }

    /**
     * set terrain method with paramater to choose terrain type
     * @param terrainType terrain type
     */
    public void setTerrain (String terrainType) {
       terrain = terrainType;
    }

    /**
     * toString method
     * @return terrain
     */
    @Override
    public String toString() {
        return terrain;
    }
}

package ntnu.idatt2001.wargames;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import java.util.Objects;

public class WargameApplication extends Application {
    /**
     * overriding start method for application
     * @param primaryStage primaryStage
     * @throws Exception exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load((Objects.requireNonNull(getClass().getClassLoader().getResource(("FXML files/FrontPage.fxml")))));
        Image image = new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("game.png")));
        primaryStage.getIcons().add(image);
        primaryStage.setTitle("Wargames!");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
    /**
     * method to open and start the application
     * @param args Args
     */
    public static void main(String[] args) {
        launch(args);
    }
}

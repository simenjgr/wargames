package ntnu.idatt2001.wargames;

import ntnu.idatt2001.wargames.units.Units;

/**
 * represents a battle between two armies
 */
public class Battle {

    private Army one;
    private Army two;
    private final Terrain terrain = new Terrain();

    /**
     * constructor with relevant paramteters, adds two armies into a battle
     * @param one army one
     * @param two army two
     */
    public Battle(Army one, Army two) {
        this.one = one;
        this.two = two;
    }

    /**
     * simulates a battle between two armies
     * @param chooseTerrain adds terrain to the simulation between the armies
     * @return returns army with units left
     */
    public Army simulate(String chooseTerrain) {

        terrain.setTerrain(chooseTerrain);

        while (one.hasUnits() && two.hasUnits()) {

            Units army1unit = (Units) one.getRandom();
            Units army2unit = (Units) two.getRandom();

            army1unit.attack(army2unit);
            army2unit.attack(army1unit);

            if (army1unit.getHealth() < 1) {
                one.removeUnit(army1unit);
            }
            if (army2unit.getHealth() < 1) {
                two.removeUnit(army2unit);
            }
        }

        if (!one.hasUnits()) {
            return two;
        }
        else { return one;
        }
    }

    /**
     * toString method
     * @return army one and army two
     */
    @Override
    public String toString() {
        return  "one=" + one +
                ", two=" + two +
                '}';
    }
}

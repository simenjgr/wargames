package ntnu.idatt2001.wargames.units;

/**
 * abstract class Units
 * each unit consist of name, health, attack and armor
 */
public abstract class Units {
        private String name;
        private String type;
        private int health, attack, armor;

    /**
     * constructor for Units
     * @param name name of unit
     * @param health health of unit
     * @param attack attack damage of unit
     * @param armor armor of unit
     */
     public Units (String name, int health, int attack, int armor, String type) {
            this.name = name;
            this.health = health;
            this.attack = attack;
            this.armor = armor;
            this.type = type;
     }

    /**
     * the logic that handle each attack between units
     * @param opponent creates an opponent
     */
     public void attack (Units opponent) {
      opponent.health = opponent.health - (this.attack + this.getAttackBonus()) + (opponent.armor + opponent.getResistBonus());
     }

    /**
     * gets name of unit
     * @return name
     */
     public String getName() {
         return name;
     }

    /**
     * gets health of unit
     * @return health
     */
     public int getHealth() {
         return (health);
     }

     public String getType(){
         return type;
     }

    /**
     * get attack damage of unit
     * @return attack
     */
     public int getAttack() {
         return attack;
     }

    /**
     * get armor of unit
     * @return armor
     */
     public int getArmor() {
         return armor;
     }

    /**
     * set health of unit to int value typed in param
     * @param health health
     */
     public void setHealth(int health) {
         this.health = health;
     }

    /**
     * toString method returns name and health
     * @return name
     * @return health
     */
    @Override
     public String toString() {
         return  "\n" + type +name+","+health +"\n";
     }

    /**
     * other units inherit abstract method of attack bonus
     */
     public abstract int getAttackBonus ();
    /**
     * other units inherit abstract method of resist bonus
     */
     public abstract int getResistBonus();
}

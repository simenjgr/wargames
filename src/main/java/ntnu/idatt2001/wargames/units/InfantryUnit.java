package ntnu.idatt2001.wargames.units;

import ntnu.idatt2001.wargames.Terrain;
import java.util.Objects;

/**
 * represents the infantry unit with individual traits
 */
public class InfantryUnit extends Units {

    private final Terrain terrain = new Terrain();

    /**
     * constuctor to infantry unit with relevant paramaters
     * @param name name of unit
     * @param health health of unit
     * @param attack attack of unit
     * @param armor armor of unit
     */
    public InfantryUnit(String name, int health, int attack, int armor, String type) {
        super(name, health, attack, armor, type);
    }

    /**
     * constructor to infantry unit with relevant parameters, attack set to 15 and armor to 9
     * @param name name of unit
     * @param health health of unit
     */
    public InfantryUnit(String name, int health, String type) {
        super(name, health, 15, 9, type);
    }

    /**
     * gives the unit an attack bonus, more bonus when terrain is set to forrest
     * @return bonus damage
     */
    @Override
    public int getAttackBonus() {
        if (Objects.equals(terrain.getTerrain(), "Forest")) {
            return 4;
        } else {
            return 2;
        }
    }

    /**
     * gives the unit a resist bonus, more resist bonus when terrain is set to forest
     * @return resistens bonus
     */
    @Override
    public int getResistBonus() {
        if (Objects.equals(terrain.getTerrain(), "Forest")) {
            return 3;
        } else {
            return 1;
        }
    }
}


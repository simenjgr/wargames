package ntnu.idatt2001.wargames.units;

/**
 * represents the commander unit
 */
public class CommanderUnit extends CavalryUnit {
    /**
     * cunstructor with relevant paramters for the unit
     * @param name name of unit
     * @param health health of unit
     * @param attack attack damage of unit
     * @param armor armor of unit
     */
    public CommanderUnit(String name, int health, int attack, int armor, String type) {
        super(name, health, attack, armor, type);
    }

    /**
     * cunstructor with relevant paramters for the unit, attack damage set to 25 and armor to 8
     * @param name name of unit
     * @param health health of unit
     */
    public CommanderUnit(String name, int health, String type) {
        super(name, health, 25 , 8, type);
    }
}

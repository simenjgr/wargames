package ntnu.idatt2001.wargames.units;

import ntnu.idatt2001.wargames.Terrain;
import java.util.Objects;

/**
 * represents the Cavalry unit
 */
public class CavalryUnit extends Units {

    static int count = 0;
    Terrain terrain = new Terrain();

    /**
     * constructor of relevant paramters
     * @param name name of unit
     * @param health health of unit
     * @param attack attack damage of unit
     * @param armor armor of unit
     */
    public CavalryUnit(String name, int health, int attack, int armor, String type) {
        super(name, health, attack, armor, type);
    }

    /**
     * constructor of relevant paramters, attack damage set to 20 and armor to 7
     * @param name name of unit
     * @param health health of unit
     */
    public CavalryUnit(String name, int health, String type) {
        super(name, health, 20, 7, type);
    }

    /**
     * gives the cavalry unit attack bonus, more bonus if terrain is in Plains, if not it will also get a bonus first time it charges/attacks
     * @return attack bonus
     */
    @Override
    public int getAttackBonus() {
        if (Objects.equals(terrain.getTerrain(), "Plains")) {
            return 8;
        } else {
            count++;
            if (count == 1) {
                return 6;
            } else return 2;
        }
    }

    /**
     * gives the cavalry unit resist bonus, zero bonus is given in terrain 'Forest' else its given one bonus point
     * @return resist bonus
     */
    @Override
    public int getResistBonus() {
        if (Objects.equals(terrain.getTerrain(), "Forest")) {
            return 0;
        }
        else {
            return 1;
        }
    }
}

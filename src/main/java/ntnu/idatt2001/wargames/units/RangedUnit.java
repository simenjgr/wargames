package ntnu.idatt2001.wargames.units;

import ntnu.idatt2001.wargames.Terrain;
import ntnu.idatt2001.wargames.units.Units;

import java.util.Objects;

/**
 * represents a Ranged Unit with individual traits
 */
public class RangedUnit extends Units {

    static int count = 0;
    private final Terrain terrain = new Terrain();

    /**
     * constructor with relevant paramteres
     * @param name name of unit
     * @param health health of unit
     * @param attack attack damage of unit
     * @param armor armor of unit
     */
    public RangedUnit(String name, int health, int attack, int armor, String type) { super(name, health, attack, armor, type);
    }

    /**
     * constructor with relevant paramters
     * @param name name of unit
     * @param health health of unit
     */
    public RangedUnit(String name, int health, String type) {
        super(name, health, 10, 7, type);
    }

    /**
     * gives the unit attack bonus, more bonus if in Hill or Forest terrain
     * @return attack bonus
     */
    @Override
    public int getAttackBonus() {
        if (Objects.equals(terrain.getTerrain(), "Hill")) {
            return 6;
        } else if (Objects.equals(terrain.getTerrain(), "Forest")) {
            return 4;
        } else {
            return 3;
        }
    }

    /**
     * gives the unit resist bonus, more bonus the first and second time its hit by an attack
     * @return resist bonus
     */
    @Override
    public int getResistBonus() {
        count++;
        if (count <= 1) {
            return 6;
        }
        if (count == 2) {
            return 4;
        }
        else return 2;
    }
}

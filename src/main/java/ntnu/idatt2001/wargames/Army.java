package ntnu.idatt2001.wargames;

import ntnu.idatt2001.wargames.units.Units;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * the army class represents multible units combined into one army
 */
public class Army {

    private String name;
    private ArrayList <Units> unitsList;

    /**
     * constructor with relevant parameters
     * @param name name of army
     */
    public Army (String name) {
        this.name = name;
        unitsList = new ArrayList<>();
    }

    /**
     * constructor with relevant paramters
     * @param name name of army
     * @param unitsList list of units
     */
    public Army (String name, ArrayList unitsList) {
        this.name = name;
        this.unitsList = unitsList;
    }

    /**
     * get name of army
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * adds a unit to unit list
     * @param Unit unit
     */
    public void addUnit(Units Unit) {
        unitsList.add(Unit);
    }

    /**
     * adds all units to units list
     * @param unit unit
     */
    public void addAllUnits(ArrayList<Units> unit) {
        unitsList.addAll(unit);
   }

    /**
     * removes a unit from units list
     * @param unit unit
     */
    public void removeUnit (Units unit) {
        unitsList.remove(unit);
    }

    /**
     * remove all units from unitlist
     */
    public void removeAllUnits () {
        unitsList.clear();
    }

    /**
     * checks if the unit list cointains unist
     * @return false if list is empty or true if the list contains units
     */
    public boolean hasUnits () {
        if (unitsList.isEmpty()){
           return false;
        }
        return true;
    }

    /**
     * gets all the units from the list of units
     * @return all units
     */
    public ArrayList<Units> getAllUnits () {
        ArrayList<Units> units = new ArrayList<>(unitsList);
        return (units);
    }

    /**
     * gets a random object from the units list
     * @return unit
     */
    public Object getRandom () {
        if (unitsList.size() > 0) {
            Random random = new Random();
            int test = random.nextInt(unitsList.size());
            Units unit = unitsList.get(test);
            return unit;
        }
       return "\n"+"One or less items in the list, add more units to the list to run the random method!";
    }

    /**
     * gets all the infantry units from the arraylist
     * @return infantryunits
     */
    public ArrayList<Units> getInfantryUnits() {
        ArrayList<Units> infantryUnits;
        infantryUnits = unitsList.stream().filter(units -> units.getName().equals("Legionnaire")).collect(Collectors.toCollection(ArrayList::new));
        return infantryUnits;
    }

    /**
     * gets all the cavalry units from the arraylist
     * @return cavalry units
     */
    public ArrayList<Units> getCavalryUnits() {
        ArrayList<Units> cavalryUnits = unitsList.stream().filter(liste -> Objects.equals(liste.getName(), "Black rider")).collect(Collectors.toCollection(ArrayList::new));
        return cavalryUnits;
    }

    /**
     * gets all the ranged units from the arraylist
     * @return ranged units
     */
    public ArrayList<Units> getRangedUnits() {
        ArrayList<Units> rangedUnits = unitsList.stream().filter(liste -> Objects.equals((liste.getName()), "Legolas")).collect(Collectors.toCollection(ArrayList::new));
        return rangedUnits;
    }

    /**
     * gets all the commander units fom the arraylist
     * @return commander units
     */
    public ArrayList<Units> getCommanderUnits () {
        ArrayList<Units> commanderUnits;
        commanderUnits = unitsList.stream().filter(units -> units.getName().equals("Champion")).collect(Collectors.toCollection(ArrayList::new));
        return commanderUnits;
    }

    /**
     * checks if two objects are equal
     * @param o object
     * @return true or false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return name.equals(army.name) && unitsList.equals(army.unitsList);
    }

    /**
     * hashcode of object
     * @return objects.hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, unitsList);
    }

    /**
     * toString method
     * @return name and list of units
     */
    @Override
    public String toString() {
        return name+unitsList;

    }
}

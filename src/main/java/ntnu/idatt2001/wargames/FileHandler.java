package ntnu.idatt2001.wargames;

import ntnu.idatt2001.wargames.units.Units;

import java.io.*;
import java.util.Scanner;

/**
 * filehanding class to create .csv files or to read from .csv files
 */
public class FileHandler {

    /** writes an army to a .csv file
     *
     * @param file location of file
     * @param army takes in an army
     */
    public static void writeArmytoFile(File file, Army army)  {
        try (FileWriter fileWriter = new FileWriter(file, false)) {
            fileWriter.write(army.getName()+"\n");
            for (Units unit : army.getAllUnits()){
                fileWriter.write(unit.getType()+","+unit.getName()+","+unit.getHealth()+"\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * reads a .csv file and converts it to a string
     * @param path path
     * @return returns a string
     * @throws FileNotFoundException file not found exception
     */
    public static String readFromList (String path) throws FileNotFoundException {
        File file = new File (path);
        Scanner scanner = new Scanner(file);
        String line = "";
        while (scanner.hasNextLine()) {
            line = line.concat(scanner.nextLine()+ "\n");
        }
        return line;
    }
}
